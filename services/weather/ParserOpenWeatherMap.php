<?php


namespace services\weather;


/**
 * Class ParserOpenWeatherMap
 * @package services\weather
 */
class ParserOpenWeatherMap
{
    private array $data; //данные по погоде

    /**
     * ParserOpenWeatherMap constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Формирует данные для отправки на основании массива $this->data
     *
     * @return string[]
     */
    public function parseCurrentWeather(): array
    {
        return [
            'photo' => $this->getIcon4x(),
            'text' =>
              'Сейчас в городе ' . $this->getCity() . PHP_EOL .
              $this->getWeather() . PHP_EOL .
              'Температура ' . $this->data['main']['temp'] . "\u{00B0}" . PHP_EOL .
              'Ощущается как ' . $this->data['main']['feels_like'] . "\u{00B0}" . PHP_EOL .
              'Минимальная ' . $this->data['main']['temp_min'] . "\u{00B0}" . PHP_EOL .
              'Максимальная ' . $this->data['main']['temp_max'] . "\u{00B0}" . PHP_EOL .
              'Ветер ' . $this->getWindDirection() . PHP_EOL . ' дует со скоростью ' . $this->data['wind']['speed'] . ' метров в секунду',
        ];
    }

    /**
     * Возвращает полную Html верстку для Города по всем параметрам
     *
     * @return string
     */
    public function getHtmlAll(): string
    {
        return $this->getCity() !== 'No City'
            ? "<div>Сейчас в городе " . $this->getCity() . ": </div>" .
              "<div>Температура " .  $this->data['main']['temp'] . "\u{00B0}" . "</div>" .
              "<div>Ощущается как " .   $this->data['main']['feels_like'] . "\u{00B0}" . "</div>" .
              "<div>Минимальная " .  $this->data['main']['temp_min'] . "\u{00B0}" . "</div>" .
              "<div>Максимальная " .  $this->data['main']['temp_max'] . "\u{00B0}" . "</div>" .
              "<div>Ветер " .  $this->getWindDirection() . "</div>" .
              "<div>дует со скоростью " .  $this->data['main']['temp'] . " метров в секунду</div>"
            : "<div>Город не найден</div>";
    }

    /**
     * Возвращает Html верстку для Температуры
     *
     * @return string
     */
    public function getHtmlTemperature(): string
    {
        return $this->getCity() !== 'No City'
            ? "<div>Температура " .  $this->data['main']['temp'] . "\u{00B0}" . "</div>"
            : "";
    }

    /**
     * Возвращает Html верстку для Города
     *
     * @return string
     */
    public function getHtmlCity(): string
    {
        return $this->getCity() !== 'No City'
            ? "<div>Сейчас в городе " . $this->getCity() . ": </div>"
            : "<div>Город не найден</div>".
              "<div>Попробуйте еще раз</div>";
    }

    /**
     * Возвращает название города или "No City", если город не найден
     *
     * @return string
     */
    private function getCity(): string
    {
        return $this->data['name'] ?? "No City";
    }

    /**
     * Возвращает ссылку на стандартную иконку
     *
     * @return string
     */
    public function getIcon(): string
    {
        return isset($this->data['weather'][0]['icon'])
            ? 'https://openweathermap.org/img/wn/' . $this->data['weather'][0]['icon'] . '.png'
            : "No icon";
    }

    /**
     * Возвращает ссылку на иконку двойного размера
     *
     * @return string
     */
    public function getIcon2x(): string
    {
        return isset($this->data['weather'][0]['icon'])
            ? 'https://openweathermap.org/img/wn/' . $this->data['weather'][0]['icon'] . '@2x.png'
            : "No icon";
    }

    /**
     * Возвращает ссылку на иконку 4х размера
     *
     * @return string
     */
    public function getIcon4x(): string
    {
        return isset($this->data['weather'][0]['icon'])
            ? 'https://openweathermap.org/img/wn/' . $this->data['weather'][0]['icon'] . '@4x.png'
            : "No icon";
    }

    /**
     * Возвращает погоду или "No weather", если погода не найдена
     *
     * @return string
     */
    private function getWeather(): string
    {
        return $this->data['weather'][0]['main'] ?? "No weather";
    }

    /**
     * возвращает текстовое значение направления ветра
     *
     * @return string
     */
    private function getWindDirection(): string
    {
        if (!isset($this->data['wind']['speed'])) {
            return "No direction";
        }
        $cardinalDirections = [
            0   => 'Северный',
            23  => 'Северо-восточный',
            68  => 'Восточный',
            113 => 'Юго-восточный',
            158 => 'Южный',
            203 => 'Юго-западный',
            248 => 'Западный',
            293 => 'Северо-западный',
            338 => 'Северный',
        ];
        $resultDegree = 0;
        foreach ($cardinalDirections as $degree => $direction) {
            if ($this->data['wind']['deg'] < $degree) {
                break;
            }
            $resultDegree = $degree;
        }
        return $cardinalDirections[$resultDegree];
    }
}