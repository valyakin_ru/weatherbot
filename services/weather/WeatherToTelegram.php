<?php


namespace services\weather;


use api\weather\OpenWeatherMap;
use exceptions\BaseException;
use Social\SocialNetworkBotSortingNode;
use Social\Telegram\TelegramBotSender;


class WeatherToTelegram
{

    /**
     * Переправляет данных из API погоды в Telegram
     *
     * @param $city
     * @param $chat_id
     * @throws BaseException
     */
    public static function transfer($city, $chat_id)
    {
        // получаем погоду
        $weatherApi = new OpenWeatherMap();
        $weather = $weatherApi->getWeather($city);
        if ($weather['cod'] != 200) {
            if ($weather['cod'] == 404) {
                SocialNetworkBotSortingNode::send(
                    new TelegramBotSender(
                        $_ENV['TELEGRAM_TOKEN'],
                        $chat_id),
                    $_ENV['WEATHER_NOCITY_ERROR_MESSAGE'],
                );
            }
            throw new BaseException($weather['message'], $weather['cod']);
        }
        $parser = new ParserOpenWeatherMap($weather);
        foreach ($parser->parseCurrentWeather($weather) as $type => $value) {
            // оправляем в телеграмм
            SocialNetworkBotSortingNode::send(
                new TelegramBotSender(
                    $_ENV['TELEGRAM_TOKEN'],
                    $chat_id),
                $value,
                $type
            );
        }
    }
}