<?php


namespace api\weather;


class OpenWeatherMap
{
    const URL = 'api.openweathermap.org/data/2.5/weather';

    private array $options = [
        'appId'     => 'Место для токена',
        'q'         => 'Город',
        'units'     => 'metric',
        'lang'      => 'en',
    ];

    /**
     * OpenWeatherMap constructor.
     */
    public function __construct()
    {
        $this->options['appId'] = $this->getToken();
    }

    /**
     * Возвращает токен
     *
     * @return string
     */
    private function getToken(): string
    {
        return $_ENV['WEATHER_TOKEN'];
    }

    /**
     * возвращает url ресурса OpenWeatherMap
     *
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['WEATHER_URL'] ?? self::URL;
    }

    /**
     * Возвращает результат обращения к API погоды
     *
     * @param string $city
     * @return mixed
     */
    public function getWeather(string $city)
    {
        $this->options['q'] = $city;
        $weather = curl_init();
        curl_setopt($weather, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($weather, CURLOPT_URL, $this->getUrl() . "?" . http_build_query($this->options));
        $response = curl_exec($weather);
        curl_close($weather);
        return json_decode($response, true);
    }
}