<?php


use exceptions\BaseException;
use services\weather\WeatherToTelegram;


/**
 * Class weather
 * !!!Обязательно без неймспеса
 */
class weather
{
    private array $data; // данные пришедшие из Telegram
    private string $chat_id; // номер чата в Telegram

    /**
     * weather constructor.
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->chat_id = $this->data['chat']['id'];
    }

    /**
     * Точка входа для исполнения команды /weather
     * @param array $params
     * @throws BaseException
     */
    public function run(array $params)
    {
        $service = new WeatherToTelegram();
        $service->transfer($params[0] ?? $_ENV['WEATHER_DEFAULT_CITY'], $this->chat_id);
    }
}