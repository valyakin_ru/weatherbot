<?php


use api\telegram\starter;
use Social\SocialNetworkBotSortingNode;
use Social\Telegram\TelegramBotSender;


/**
 * Class stop
 * !!!Обязательно без неймспеса
 */
class stop
{
    const STOPPED_MSG = "Бот остановлен. Для запуска используйте команду /start";

    private array $data; // данные пришедшие из Telegram
    private string $chat_id; // номер чата в Telegram

    /**
     * stop constructor.
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->chat_id = $this->data['chat']['id'];
    }

    /**
     * Точка входа для исполнения команды /stop
     *
     * @param array $params
     */
    public function run(array $params)
    {
        starter::letStop($this->chat_id);
        $message = self::STOPPED_MSG;
        SocialNetworkBotSortingNode::send(
            new TelegramBotSender(
                $_ENV['TELEGRAM_TOKEN'],
                $this->chat_id),
            $message,
        );
    }
}