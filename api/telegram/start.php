<?php


use api\telegram\starter;
use Social\SocialNetworkBotSortingNode;
use Social\Telegram\TelegramBotSender;


/**
 * Class start
 * !!!Обязательно без неймспеса
 */
class start
{
    const ALREADY_RUN_MSG = "Бот уже запущен и ждет ваших указаний";
    const JUST_RUN_MSG = "Бот успешно запущен!";

    private array $data; // данные пришедшие из Telegram
    private string $chat_id; // номер чата в Telegram

    /**
     * start constructor.
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->chat_id = $this->data['chat']['id'];
    }

    /**
     * Точка входа для исполнения команды /start
     *
     * @param array $params
     */
    public function run(array $params)
    {
        if (!starter::checkStartStatus($this->chat_id)) {
            starter::letStart($this->chat_id);
            $message = self::JUST_RUN_MSG;
        } else {
            $message = self::ALREADY_RUN_MSG;
        }
        SocialNetworkBotSortingNode::send(
            new TelegramBotSender(
                $_ENV['TELEGRAM_TOKEN'],
                $this->chat_id),
            $message,
        );
    }
}