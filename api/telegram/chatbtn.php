<?php


use Social\SocialNetworkBotSortingNode;
use Social\Telegram\TelegramBotSender;


/**
 * Class chatbtn
 * !!!Обязательно без неймспеса
 */
class chatbtn
{
    private array $data;
    private string $chat_id;

    /**
     * chatbtn constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->chat_id = $this->data['chat']['id'];
    }

    public function run(array $params)
    {
        $message_city1 = [
            'text' => 'Близкие города',
            'reply_markup' => [
                'resize_keyboard' => true,
                'inline_keyboard' => [
                    [
                        ['text' => 'Москва',    'callback_data' => '/weather moscow'],
                        ['text' => 'Одинцово',  'callback_data' => '/weather odintsovo'],
                        ['text' => 'Ярославль', 'callback_data' => '/weather yaroslavl'],
                        ['text' => 'Чебоксары', 'callback_data' => '/weather cheboksary'],
                        ['text' => 'Питер', 'callback_data' => '/weather petersburg'],
                    ],
                ]
            ]
        ];
        $message_city2 = [
            'text' => 'Теплые города',
            'reply_markup' => [
                'resize_keyboard' => true,
                'inline_keyboard' => [
                    [
                        ['text' => 'Сочи',          'callback_data' => '/weather sochy'],
                        ['text' => 'Севастополь',   'callback_data' => '/weather sevastopol'],
                        ['text' => 'Ялта',          'callback_data' => '/weather yalta'],
                        ['text' => 'Каир',          'callback_data' => '/weather Cairo'],
                    ],
                ]
            ]
        ];
        $message_city3 = [
        'text' => 'Зарубежные города',
        'reply_markup' => [
            'resize_keyboard' => true,
            'inline_keyboard' => [
                [
                    ['text' => 'Берлин',    'callback_data' => '/weather berlin'],
                    ['text' => 'Париж',     'callback_data' => '/weather paris'],
                    ['text' => 'Прага',     'callback_data' => '/weather prague'],
                    ['text' => 'Мадрид',    'callback_data' => '/weather madrid'],
                    ['text' => 'Лондон',    'callback_data' => '/weather london'],
                ],
            ]
        ]
    ];
        SocialNetworkBotSortingNode::send(
            new TelegramBotSender(
                $_ENV['TELEGRAM_TOKEN'],
                $this->chat_id),
            $message_city1,
            "button"
        );
        SocialNetworkBotSortingNode::send(
            new TelegramBotSender(
                $_ENV['TELEGRAM_TOKEN'],
                $this->chat_id),
            $message_city2,
            "button"
        );
        SocialNetworkBotSortingNode::send(
            new TelegramBotSender(
                $_ENV['TELEGRAM_TOKEN'],
                $this->chat_id),
            $message_city3,
            "button"
        );
    }
}