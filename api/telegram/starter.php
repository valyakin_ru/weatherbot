<?php


namespace api\telegram;


class starter
{
    const STATUS_FILE = "chats.status";

    /**
     * Проверяет запущен ли Бот для данного чата
     *
     * @param $chat_id
     * @return bool
     */
    public static function checkStartStatus($chat_id): bool
    {
        $chats = self::getChatStatusData();
        return !empty($chats[$chat_id]) && $chats[$chat_id] == '1';
    }

    /**
     * Запускает Бот для данного чата
     *
     * @param $chat_id
     */
    public static function letStart($chat_id)
    {
        $chats = self::getChatStatusData();
        $chats[$chat_id] = '1';
        self::setChatStatusData($chats);
    }

    /**
     * Останавливает Бот для данного чата
     *
     * @param $chat_id
     */
    public static function letStop($chat_id): void
    {
        $chats = self::getChatStatusData();
        $chats[$chat_id] = '0';
        self::setChatStatusData($chats);
    }

    /**
     * Возвращает массив со статусами запуска ботов
     *
     * @return array
     */
    private static function getChatStatusData(): array
    {
        $pathToFile = __DIR__."/".self::STATUS_FILE;
        if (file_exists($pathToFile)) {
            $fileData = file_get_contents($pathToFile);
        }
        return isset($fileData) ? json_decode($fileData, true) : [];
    }

    /**
     * Сохраняет массив со статусами запуска ботов
     *
     * @param $chats
     */
    private static function setChatStatusData($chats): void
    {
        $pathToFile = __DIR__."/".self::STATUS_FILE;
        file_put_contents($pathToFile, json_encode($chats));
    }
}