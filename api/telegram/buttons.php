<?php


use Social\SocialNetworkBotSortingNode;
use Social\Telegram\TelegramBotSender;


/**
 * Class buttons
 * !!!Обязательно без неймспеса
 */
class buttons
{
    private array $data;
    private string $chat_id;

    /**
     * buttons constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->chat_id = $this->data['chat']['id'];
    }

    /**
     * Точка входа в исполнение команды /buttons
     * @param array $params
     */
    public function run(array $params)
    {
        $message = [
            'text' => 'Кнопки добавлены в панель',
            'reply_markup' => [
                'resize_keyboard' => true,
                'keyboard' => [
                    [
                        ['text' => '/start'],
                        ['text' => '/stop'],
                        ['text' => '/info'],
                    ],
                    [
                        ['text' => '/weather moscow'],
                        ['text' => '/weather odintsovo'],
                        ['text' => '/chatbtn'],
                    ],
                ]
            ]
        ];
        SocialNetworkBotSortingNode::send(
            new TelegramBotSender(
                $_ENV['TELEGRAM_TOKEN'],
                $this->chat_id),
            $message,
            "button"
        );
    }
}