<?php


namespace api\telegram;


use exceptions\BaseException;
use Social\SocialNetworkBotSortingNode;
use Social\Telegram\TelegramBotSender;

/**
 * Class app
 * @package api\telegram
 */
class app {
    /**
     * Данные из Телеграмм
     * @var array|null
     */
    private ?array $data;

    private ?string $chatId;

    private ?string $message;


    /**
     * Точка входа
     *
     * @throws BaseException
     */
    public function run(): void
    {
        $this->data = $this->getData();
        if (isset($_ENV['TELEGRAM_INCOME_DATA_LOG'])&&($_ENV['TELEGRAM_INCOME_DATA_LOG']=='1')) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/log/telegram_income_data.log', date(DATE_RFC1123)." | ".print_r($this->data,true)."\n", FILE_APPEND);
        }
        $this->chatId = $this->getChatId();
        $this->data['chat']['id'] = $this->chatId; // из-за несоответсвия колбека и обычного ответа телеграмм
        $this->startCommand($this->getCommand());
    }

    /**
     * Возвращает данные пришедшие из телеграмм
     *
     * @return array
     */
    private function getData(): ?array
    {
        $data = json_decode(file_get_contents('php://input'), true);
        return $data['callback_query'] ?? $data['message'];
    }

    /**
     * Возвращает сообщение из чата (колбек или обычное)
     *
     * @return string
     */
    private function getMessage(): ?string
    {
        return $this->data['data'] ?? $this->data['text'];
    }

    /**
     * Возвращает Id чата
     *
     * @return string
     */
    private function getChatId(): ?string
    {
        return $this->data['message']['chat']['id'] ?? $this->data['chat']['id'];
    }

    /**
     * Извлекает комманду из набора данных
     *
     * @return array
     */
    private function getCommand():array
    {
        if (!$this->data) {
            return ["'NoCommand'"];
        }
        $text = mb_strtolower($this->getMessage(), 'utf-8');
        $command = explode(" ", $text);
        //$command[0] = explode("/", $command[0])[1];
        $command[0] = explode("@", $command[0])[0];
        return $command;
    }

    /**
     * Загружает класс и запускает метод run - точка входа
     *
     * @param array $command
     * @return bool
     * @throws BaseException
     */
    private function startCommand(array $command): bool
    {
        $class = array_shift($command);
        if (substr($class, 0,1) != "/") {
            return false;
        }
        if (file_exists(__DIR__.$class.".php")) {
            require_once __DIR__.$class.".php";
            $class = substr($class, 1);
            if (class_exists($class)
                && ($class == 'start' || starter::checkStartStatus($this->chatId))) {
                try {
                    $app = new $class($this->data);
                    $app->run($command);
                } catch (Exception $exception) {
                    http_response_code($exception->getCode());
                    echo "Code:" . $exception->getCode() . ". " . $exception->getMessage() . PHP_EOL;
                }
            } else {
                throw new BaseException("Command $class was not executed. Class for command $class not found", 404);
            }
        } else {
            SocialNetworkBotSortingNode::send(
                new TelegramBotSender(
                    $_ENV['TELEGRAM_TOKEN'],
                    $this->chatId),
                $_ENV['TELEGRAM_NOCOMMAND_ERROR_MESSAGE'],
            );
            throw new BaseException("Command $class was not executed. File for $class not found", 400);
        }
        return true;
    }
}
