<?php

use Social\SocialNetworkBotSortingNode;
use Social\Telegram\TelegramBotSender;


/**
 * Class info
 * !!!Обязательно без неймспеса
 */
class info
{
    private array $data;
    private string $chat_id;

    /**
     * info constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->chat_id = $this->data['chat']['id'];
    }

    /**
     * Точка входа в исполнение команды /info
     * @param array $params
     */
    public function run(array $params)
    {
        $message = $_ENV['TELEGRAM_COMMANDS']."\n".
                    "Copyright\u{00A9} Vladimir Valyakin";
        SocialNetworkBotSortingNode::send(
            new TelegramBotSender(
                $_ENV['TELEGRAM_TOKEN'],
                $this->chat_id),
            $message,
        );
    }

}