<?php
/*use api\weather\OpenWeatherMap;
use Social\Telegram\TelegramBotSender;
use cli\weather\Sender;
use services\weather\ParserOpenWeatherMap;*/
use \services\weather\WeatherToTelegram;

require __DIR__ . '/../../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__. '/../..');
$dotenv->load();

$service = new WeatherToTelegram();
try {
    $service->transfer($_ENV['WEATHER_DEFAULT_CITY'], $_ENV['TELEGRAM_DEFAULT_CHAT_ID']);
} catch (Exception $e) {
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/log/telegram_error_log', date("l dS of F Y h:I:s A")."    ".$e->getMessage()."\n", FILE_APPEND);
    http_response_code($e->getCode());
}