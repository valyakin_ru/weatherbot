<?php

use api\telegram\app;
use exceptions\BaseException;

require __DIR__ . '/../../../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable($_SERVER['DOCUMENT_ROOT']);
$dotenv->load();

/**
 * Прежде чем этот скрипт начнет работать требуется указать телеграму куда он должен слать запросы на обработку бота,
 * у которого токен 1616960218:AAHO-AEogIDjtysdymzJScT_euGzXmbh2sg
 * это мой бот @mygroupweatherbot
 * https://api.telegram.org/bot1616960218:AAHO-AEogIDjtysdymzJScT_euGzXmbh2sg/setwebhook?url=https://valyakin.ru/cli/telegram/bot/receiver.php
 */

$app = new app();
try {
    $app->run();
} catch (BaseException $e) {
    if (isset($_ENV['TELEGRAM_ERROR_LOG']) && ($_ENV['TELEGRAM_ERROR_LOG'] == '1')) {
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/log/telegram_error.log', date(DATE_RFC1123) . "    " . $e->getMessage() . "\n", FILE_APPEND);
    }
}