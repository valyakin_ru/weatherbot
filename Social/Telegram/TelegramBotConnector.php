<?php


namespace Social\Telegram;


use Exception;
use Social\Interfaces\SocialNetworkBotConnector;
use Social\dto\AbstractDto;
use Social\dto\telegram\SendMessage;
use Social\dto\telegram\SendPhoto;
use Social\dto\telegram\SendVideo;
use Social\dto\telegram\SendButton;

/**
 * Class TelegramBotConnector
 * @package Social\Telegram
 */
final class TelegramBotConnector implements SocialNetworkBotConnector
{
    const URL = 'https://api.telegram.org/bot';

    private $curl;
    private array $sendData;
    private string $chat_id;
    private string $token;

    /**
     * TelegramBotConnector constructor.
     * @param string $token токен для телеграмм бота
     * @param string $chat_id id чата куда посылаем данные
     */
    public function __construct(string $token, string $chat_id)
    {
        $this->chat_id = $chat_id;
        $this->token = $token;
    }

    /**
     * отправляет боту текстовое сообщение
     *
     * @param $content
     * @throws Exception
     */
    public function createMessage($content): void
    {
        // TODO: Implement sendMessage() method.
        $this->sendData = $this->getDataToSend(new SendMessage([
            'text' => $content,
            'chat_id' => $this->chat_id,
            'disable_notification' => true,
        ]));
        $this->sendData('message');
    }

    /**
     * отправляет боту фото
     *
     * @param $content
     * @throws Exception
     */
    public function createPhoto($content): void
    {
        // TODO: Implement sendPhoto() method.
        $this->sendData = $this->getDataToSend(new SendPhoto([
            'photo' => $content,
            'chat_id' => $this->chat_id,
            'disable_notification' => true,
        ]));
        $this->sendData('photo');
    }

    /**
     * отправляет боту Видео
     *
     * @param $content
     * @throws Exception
     */
    public function createVideo($content): void
    {
        // TODO: Implement sendVideo() method.
        $this->sendData = $this->getDataToSend(new SendVideo([
            'video' => $content,
            'chat_id' => $this->chat_id,
            'disable_notification' => true,
        ]));
        $this->sendData('video');
    }

    /**
     * отправляет боту кнопку(и)
     *
     * @param $content
     * @throws Exception
     */
    public function createButton($content): void
    {
        // TODO: Implement sendButton() method.
        $this->sendData = $this->getDataToSend(new SendButton([
            'text' => $content['text'],
            'chat_id' => $this->chat_id,
            'reply_markup' => $content['reply_markup'],
            'disable_notification' => true,
        ]));
        $this->sendData('message');
    }

    /**
     * Инициализирует соединение
     */
    private function initConnection()
    {
        $this->curl = curl_init();
        curl_setopt_array($this->curl, [
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => array_merge(array("Content-Type: application/json"), $this->getHeaders()),
        ]);
    }

    /**
     * Закрывает соединение
     */
    private function closeConnection(): void
    {
        curl_close($this->curl);
    }

    /**
     * Возвращает заголовки
     *
     * @return array
     */
    private function getHeaders(): array
    {
        return [];
    }

    /**
     * Взовращает URL доступа к телеграмму
     *
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['TELEGRAM_URL'] ?? self::URL;
    }

    /**
     * Возвращает метод для обработки телеграммом на основе $postType
     *
     * @param string $postType - тип передаваемых данных (сообщение, фото, видео и т.д. )
     * @return string
     */
    private function getMethod($postType = 'message'): string
    {
        switch ($postType) {
            case 'message':
                return 'SendMessage';
            case 'video':
                return 'sendVideo';
            case 'photo':
                return 'sendPhoto';
        }
    }

    /**
     * Возвращает данные в виде массива из набора данных dto, где содержаться различные параметры
     *
     * @param AbstractDto $dto
     * @return array
     */
    private function getDataToSend(AbstractDto $dto): array
    {
        return $dto->asArray();
    }

    /**
     * Отправляет данные находящиеся в свойстве $this->sendData в телеграмм
     *
     * @param $method - метод обработки телеграммом (sendMessage, sendPhoto и пр...)
     * @throws Exception
     */
    private function sendData($method)
    {
        $this->initConnection();
        curl_setopt_array($this->curl, [
            CURLOPT_URL => $this->getUrl() . $this->token . "/". $this->getMethod($method),
            CURLOPT_POSTFIELDS => json_encode($this->sendData),
        ]);
        $response = curl_exec($this->curl);
        if ($response === false) {
            throw new Exception("Не удалось отправить данные в телеграмм", 400);
        }
        $response = json_decode($response, true);
        if ($response['ok'] === false) {
            throw new Exception($response['description'], $response['error_code']);
        }
        $this->closeConnection();
    }
}