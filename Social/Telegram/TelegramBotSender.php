<?php


namespace Social\Telegram;

use Social;
use Social\Interfaces\SocialNetworkBotConnector;
use Social\SocialNetworkBotSender;

/**
 * Class TelegramBotSender
 * @package Social\Telegram
 * Этот Конкретный Создатель поддерживает Bot Telegram. Важно, что этот класс
 * также наследует методы sendMessage, sendPhoto, sendVideo от родительского класса. Конкретные Создатели —
 * это классы, которые фактически использует Клиент.
 */
class TelegramBotSender extends SocialNetworkBotSender
{
    private string $chat_id;
    private string $token;

    /**
     * TelegramBotSender constructor.
     * @param string $token
     * @param string $chat_id
     */
    public function __construct(string $token, string $chat_id)
    {
        $this->chat_id = $chat_id;
        $this->token = $token;
    }

    /**
     * Возвращает объект с которым будет работать клиентский код
     *
     * @return SocialNetworkBotConnector
     */
    public function getSocialNetwork(): SocialNetworkBotConnector
    {
        // TODO: Implement getSocialNetwork() method.
        return new TelegramBotConnector($this->token, $this->chat_id);
    }

}