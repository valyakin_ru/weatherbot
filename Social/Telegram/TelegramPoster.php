<?php


namespace Social\Telegram;


use Social\SocialNetworkPoster;
use Social\Interfaces\SocialNetworkConnector;

/**
 * Class TelegramPoster
 * @package Social\Telegram
 * Этот Конкретный Создатель поддерживает Telegram. Важно, что этот класс
 * также наследует метод post от родительского класса. Конкретные Создатели —
 * это классы, которые фактически использует Клиент.
 */
class TelegramPoster extends SocialNetworkPoster
{
    private string $login;
    private string $password;

    /**
     * TelegramPoster constructor.
     * @param string $login
     * @param string $password
     */
    public function __construct(string $login, string $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * Возвращает объект с которым будет работать клиентский код
     *
     * @return SocialNetworkConnector
     */
    public function getSocialNetwork(): SocialNetworkConnector
    {
        // TODO: Implement getSocialNetwork() method.
        return new TelegramConnector($this->login, $this->password);
    }

}