<?php


namespace Social\dto;


use ReflectionClass;

/**
 * Class AbstractDto
 * @package Social\dto
 */
class AbstractDto
{
    /**
     * Заполняет создаваемый объект Dto данными
     *
     * AbstractDto constructor.
     * @param null $data
     */
    public function __construct($data = null)
    {
        if ($data) {
            $this->populate($data);
        }
    }

    /**
     * Заполняет данными свойства dto на основании $data
     *
     * @param $data - данные для dto
     * @return $this
     */
    public function populate($data): AbstractDto
    {
        if (is_array($data) || is_object($data)) {
            $object = is_object($data) ? $data : (object)$data;
            foreach (array_keys($this->getProperties()) as $property) {
                $this->{$property} = $object->{$property} ?? null;
            }
        }
        return $this;
    }

    /**
     * Возвращает dto в виде массива
     *
     * @return array
     */
    public function asArray(): array
    {
        $array = [];
        foreach (array_keys($this->getProperties()) as $property) {
            $array[$property] = $this->{$property};
        }
        return $array;
    }

    /**
     * Возвращает набор свойств объекта в виде ключей массива с нулевыми значениями
     *
     * @return array
     */
    protected function getProperties(): array
    {
        $properties = [];
        foreach ((new ReflectionClass($this))->getProperties() as $property) {
            $properties[$property->getName()] = null;
        }
        // закомментировано удаление свойств со значениями по умолчанию
        /*foreach (array_keys(get_class_vars(__CLASS__)) as $property) {
            unset($properties[$property]);
        }*/
        return $properties;
    }
}