<?php


namespace Social\dto\telegram;


use Social\dto\AbstractDto;

/**
 * Class SendPhoto
 * @package Social\dto\telegram
 */
class SendPhoto extends AbstractDto
{
    /**
     * Url ссылка на фото
     *
     * @var string|null
     */
    public ?string $photo;

    /**
     * Номер чата
     *
     * @var string|null
     */
    public ?string $chat_id;

    /**
     * Звуковая нотификация появления сообщения
     * true - отключает звук
     * false - включает звук
     *
     * @var bool|null
     */
    public ?bool $disable_notification;
}