<?php


namespace Social\dto\telegram;


use Social\dto\AbstractDto;


/**
 * Class SendButton
 * @package Social\dto\telegram
 */
class SendButton extends AbstractDto
{
    /**
     * Название блока кнопок
     *
     * @var string|null
     */
    public ?string $text;

    /**
     * Номер чата
     *
     * @var string|null
     */
    public ?string $chat_id;

    /**
     * Массив настроек кнопок
     * 'reply_markup' => [
     *     'resize_keyboard' => true - изменение размера кнопок
     *     'keyboard' => [
     *         [
     *             ['text' => 'Кнопка 1'], ['text' => 'Кнопка 2'],
     *         ],
     *         [
     *             ['text' => 'Кнопка 3'], ['text' => 'Кнопка 4'],
     *         ]
     *     ]
     * ]
     * @var array|null
     */
    public ?array $reply_markup;

    /**
     * Звуковая нотификация появления сообщения
     * true - отключает звук
     * false - включает звук
     *
     * @var bool|null
     */
    public ?bool $disable_notification;

}