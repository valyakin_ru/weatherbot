<?php


namespace Social\dto\telegram;


use Social\dto\AbstractDto;

/**
 * Class SendMessage
 * @package Social\dto\telegram
 */
class SendMessage extends AbstractDto
{
    /**
     * Текст сообщения
     *
     * @var string|null
     */
    public ?string $text;

    /**
     * Номер чата
     *
     * @var string|null
     */
    public ?string $chat_id;

    /**
     * Звуковая нотификация появления сообщения
     * true - отключает звук
     * false - включает звук
     *
     * @var bool|null
     */
    public ?bool $disable_notification;
}