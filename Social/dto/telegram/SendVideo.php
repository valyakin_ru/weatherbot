<?php


namespace Social\dto\telegram;


use Social\dto\AbstractDto;


/**
 * Class SendVideo
 * @package Social\dto\telegram
 */
class SendVideo extends AbstractDto
{
    public ?string $video;
    public ?string $chat_id;
    public ?bool $disable_notification;
}