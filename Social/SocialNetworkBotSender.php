<?php


namespace Social;


use Social\Interfaces\SocialNetworkBotConnector;


/**
 * Class SocialNetworkBotSender
 * @package Social
 */
abstract class SocialNetworkBotSender
{
    /**
     * Фактический фабричный метод. Важно, что он возвращает
     * абстрактный коннектор. Это позволяет подклассам возвращать любые
     * конкретные коннекторы без нарушения контракта суперкласса.
     *
     * @return SocialNetworkBotConnector
     */
    abstract public function getSocialNetwork(): SocialNetworkBotConnector;

    /**
     * Когда фабричный метод используется внутри бизнес-логики Создателя,
     * подклассы могут изменять логику косвенно, возвращая из фабричного метода
     * различные типы коннекторов.
     *
     * @param $content
     */
    public function sendMessage($content)
    {
        $networkBot = $this->getSocialNetwork(); // возвращает объект коннектера к какой-то социальной сети
        $networkBot->createMessage($content); // выполняет метод createMessage для конкретного объекта (Телеграм, Вотсап и пр то что реализовано в виде коннектора)
    }

    /**
     * @param $content
     */
    public function sendPhoto($content)
    {
        $networkBot = $this->getSocialNetwork();
        $networkBot->createPhoto($content);
    }

    /**
     * @param $content
     */
    public function sendVideo($content)
    {
        $networkBot = $this->getSocialNetwork();
        $networkBot->createVideo($content);
    }

    /**
     * @param $content
     */
    public function sendButton($content)
    {
        $networkBot = $this->getSocialNetwork();
        $networkBot->createButton($content);
    }


}