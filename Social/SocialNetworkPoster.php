<?php


namespace Social;


use Social\Interfaces\SocialNetworkConnector;


/**
 * Class SocialNetworkPoster
 * для отправки постов залогиневшегося пользователся
 * @package Social
 */
abstract class SocialNetworkPoster
{
    /**
     * Фактический фабричный метод. Важно, что он возвращает
     * абстрактный коннектор. Это позволяет подклассам возвращать любые
     * конкретные коннекторы без нарушения контракта суперкласса.
     *
     * @return SocialNetworkConnector
     */
    abstract public function getSocialNetwork(): SocialNetworkConnector;

    /**
     * Когда фабричный метод используется внутри бизнес-логики Создателя,
     * подклассы могут изменять логику косвенно, возвращая из фабричного метода
     * различные типы коннекторов.
     *
     * @param $content
     */
    public function post($content): void
    {
        // Вызываем фабричный метод для создания объекта Продукта...
        $network = $this->getSocialNetwork();
        // ...а затем используем его по своему усмотрению.
        $network->logIn();
        $network->createPost($content);
        $network->logout();
    }
}