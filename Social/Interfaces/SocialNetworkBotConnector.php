<?php


namespace Social\Interfaces;


interface SocialNetworkBotConnector
{
    public function createMessage($content): void;

    public function createPhoto($content): void;

    public function createVideo($content): void;

    public function createButton($content): void;
}