<?php


namespace Social;


/**
 * Class SocialNetworkBotSortingNode
 * @package Social
 */
class SocialNetworkBotSortingNode
{
    /**
     * Отправляет контент в бот социальной сети
     *
     * @param SocialNetworkBotSender $creator
     * @param $content
     * @param string $contentType
     */
    public static function send(SocialNetworkBotSender $creator, $content, $contentType = "text")
    {
        switch ($contentType) {
            case "text":
                $creator->sendMessage($content);
                break;
            case "photo":
                $creator->sendPhoto($content);
                break;
            case "video":
                $creator->sendVideo($content);
                break;
            case "button":
                $creator->sendButton($content);
                break;
        }
    }
}